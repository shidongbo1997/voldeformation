function [en,grad,hess] = isometricEnergyFromELS(Js,xref,x,t,weight)
isProj = false;

nT = size(t,1);
X2V = [1 -1 0 0;1 0 -1 0; 1 0 0 -1; 0 1 -1 0; 0 1 0 -1; 0 0 1 -1];

%% get the symbol formula of grad and hess in terms of metric
syms m1 m2 m3 m4 m5 m6;
M = [m1 m2 m3;m2 m4 m5;m3 m5 m6];
E = trace(M)+trace(inv(M));
J = jacobian(E,[m1 m2 m3 m4 m5 m6]);
H = jacobian(J,[m1 m2 m3 m4 m5 m6]);

%% calc for each tet
e = zeros(nT,1);
grad = zeros(12,nT);
hess = zeros(12,12*nT);

for i=1:nT
    jacob_i = Js(3*i-2:3*i,:);
    M_i = jacob_i*jacob_i';
    M_i = M_i([1 2 3 5 6 9]);
    
    e(i) = subs(E,[m1 m2 m3 m4 m5 m6],M_i);
    Jm_i = double(subs(J,[m1 m2 m3 m4 m5 m6],M_i));
    Hm_i = double(subs(H,[m1 m2 m3 m4 m5 m6],M_i));
    
    t_i = t(i,:);
    xref_i = xref(t_i,:);
    vref_i = X2V*xref_i;
    x_i = x(t_i,:);
    v_i = X2V*x_i;
    
    % invT = dmdV2
    T_i = [vref_i(:,1).^2 2*vref_i(:,1).*vref_i(:,2) 2*vref_i(:,1).*vref_i(:,3) vref_i(:,2).^2 2*vref_i(:,2).*vref_i(:,3) vref_i(:,3).^2];
    invT_i = inv(T_i);
    
    dV2dX = 2*[v_i(:,1).*X2V v_i(:,2).*X2V v_i(:,3).*X2V];
    grad_i = weight(i)*Jm_i*invT_i*dV2dX;
    
    hess_i_1 = dV2dX'*invT_i'*Hm_i*invT_i*dV2dX;
    dEdV2 = Jm_i*invT_i;
    a1 = dEdV2(1);a2 = dEdV2(2);a3 = dEdV2(1);a4 = dEdV2(1);a5 = dEdV2(1);a6 = dEdV2(1);
    if isProj
        % [0 0;0 B] = Q*A*Q';
        B = [a1+a4+a5 -a4 -a5;-a4 a2+a4+a6 -a6;-a5 -a6 a3+a5+a6];
        [U,S,V] = svd(B);
        S(find(S<0))=0;
        B = U*S*V';
        invQ = [1 -1 -1 -1;0 1 0 0;0 0 1 0;0 0 0 1];
        hess_i_2 = 2*invQ*blkdiag(0,B)*invQ';
    else
        hess_i_2 = 2*(a1*D2V2DX(1,2)+a2*D2V2DX(1,3)+a3*D2V2DX(1,4)+...
                    a4*D2V2DX(2,3)+a5*D2V2DX(2,4)+a6*D2V2DX(3,4));
    end
    hess_i_2 = blkdiag(hess_i_2,hess_i_2,hess_i_2);
    hess_i = weight(i)*(hess_i_1 + hess_i_2);
    
    grad(:,i) = grad_i([1 5 9 2 6 10 3 7 11 4 8 12])';% d(x1x2x3x4->z1z2z3z4) --> d(x1 y1 z1 -> x4 y4 z4)
    hess(:,12*i-11:12*i) = hess_i([1 5 9 2 6 10 3 7 11 4 8 12],[1 5 9 2 6 10 3 7 11 4 8 12]);
end

end

function A = D2V2DX(i,j)
A = zeros(4,4);
A(i,i) = 1;A(j,j) = 1;
A(i,j) = -1;A(j,i) = -1;
end
